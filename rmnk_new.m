function lab()
%     trajektoria zadana
    a=zeros(1,50); b=ones(1,50); c=0.5*ones(1,30);
    d=[1:30]/30; e=1-d; f=zeros(1,50);  x = rand(1, 20);
    w=[b a f b f b f c a d e a c]';
    
%     inicjalizacja modeli
    B = [0.4; 0.2];
    A = [1; -0.4];
    k = 1;
    arx = ARXSimInit(B, A, k);
    gpc = GPCInit(1, 1, 0, w);
    rls = RLSInit(1,1,1,1);
    
%     pamiec sterowan i odpowiedz (dla wykresu)
    yy = zeros(500, 1);
    uu = zeros(500, 1);
    
%%     symulacja

%   pierwsze 20 krokow pobudzamy model szumem, w celu rozchulania
%   identyfikacji
    for i = 1:20
        u = rand(1, 1);
        arx = ARXSimStep(arx, u);
        rls = RLSStep(rls, u, arx.y);
    end
    
%    wlasciwa symulacja 
    for i = 1:length(w);
        
%         ustawiamy model referencyjny uzyskany z identyfikacji
        [num den] = RLSData(rls);
        gpc.model = ARXSimInit(num, den, rls.k);
%         lub podajemy dokladny model
%         gpc.model = ARXSimInit(B, A, k);
        
%         obliczamy sterowanie 
        gpc = GPCStep(gpc, arx.y);
        u = gpc.u;
        
%         wyznaczamy odpowiedz obiektu regulacji
        arx = ARXSimStep(arx, u);
        
%         krok identyfikacji
        rls = RLSStep(rls, u, arx.y);

%         zapisanie zmienncyh do bufora w celu ich wysweitlenia
        yy(i) = arx.y;
        uu(i) = u;
    end
    
%% wykres

    plot(w, 'b');
    hold on;
    plot(yy,'r -');  
    plot(uu, 'g -.')
    
    legend({'wartosc zadana', 'y(i)', 'u(i)'})
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%   GPC   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inicjalizacja gpc
function gpc = GPCInit(H, L, ro, w)
%     wydluzenie wektora w o H, aby moc doprowadzic identyfikacje do konca
    w = [w' ones(1, H)*w(end)]';

    gpc = struct('H', H, 'L', L, 'ro', ro, 'w', w, 'iteration', 1, 'du', 0, 'u', 0, 'prevDy', 0, 'prevDu', 0, 'lastY', 0, 'model', 0);
end

% krok gpc
function res = GPCStep(gpc, y)  

% ustawienie modelu
    dA = 1;
    dB = 1;
    modelB = gpc.model.B;
    modelA = gpc.model.A;
    k = gpc.model.k-1;

% inicjalizacja wektorow poprzednich zmian y i u (zakladamy ze struktura
% modelu nie zmienia sie w trakcie pracy)
    if(gpc.iteration == 1)
        gpc.prevDy  = zeros(dA, 1);
        gpc.prevDu = zeros(dB+k, 1);
    end

%     wyznaczenie odpowiedzi skokowej referencyjnego modelu
    step = ones(gpc.H, 1);
    h = filter(modelB, modelA, step);
    
    
%     wyznaczenie wartosi Q
    Q = zeros(gpc.H, gpc.L);
    for i=1:gpc.H
        for j = 1:gpc.L
            index = i - j + 1;
            if(index < 1)
                Q(i, j) = 0;
            else
                Q(i, j) = h(index);
            end
        end
    end

%     wyznaczenie wektora w0
    w0 = gpc.w(gpc.iteration+1:gpc.iteration+gpc.H);
    
    
%     aktualizacja wektora zmian poprzednich odpowiedzi
    gpc.prevDy = [(y - gpc.lastY) gpc.prevDy(1:end-1)']';

%     liczenie y0
    y0 = zeros(gpc.H, 1);
    xx = zeros(gpc.H+1, 1);
    xx(1) = gpc.du;
    yy = filterWithIC(modelB, modelA, k, xx, gpc.prevDu, gpc.prevDy);
    temp = yy(1) + y;
    y0(1) = yy(2) + temp;
    for i=2:gpc.H
        y0(i) = yy(i+1) + y0(i-1);
    end
    
%     aktualizacja wektora bylych zmian sterowania
    gpc.prevDu = [gpc.du gpc.prevDu(1:end-1)']';
    
%     obliczenie q
    one = zeros(gpc.L, 1);
    one(1) = 1;
    q = one'*inv(Q'*Q +gpc.ro*eye(gpc.L))*(Q');

%     wyznaczenie sterowania
    gpc.du = q*(w0 - y0);
    gpc.u = gpc.u + gpc.du;
    
    
%   aktaulizacja stanow
    gpc.iteration=gpc.iteration+1;
    gpc.lastY = y;
    
%     zwrocenie gpc
    res = gpc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%   Obiekt ARX   %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inicjalizacja obiektu arx
function arx = ARXSimInit(B, A, k)

    dB = length(B)-1;
    dA = length(A)-1;
    
    arx = struct('B', B, 'A', A, 'k', k, 'prevX', zeros(dB+k, 1), 'prevY', zeros(dA, 1), 'y', 0);
end

% krok symulacji obiektu arx
function arx = ARXSimStep(arx, x)
    arx.y = filterWithIC(arx.B, arx.A, arx.k, x, arx.prevX, arx.prevY);
    
    arx.prevX = [x arx.prevX(1:end-1)']';
    arx.prevY = [arx.y arx.prevY(1:end-1)']';
end

% bonus: odpowiednik funkcji filtrer, przyjmujacy na wejsciu dodatkowo
% opoznienie k oraz warunki poczatkowe x0 i y0
function res = filterWithIC(aB, A, k, x, x0, y0)
    res = zeros(length(x), 1);
    B = [zeros(1, k) aB']';
    
    for i = 1:length(x)
        y = B(1)'*x(i) - A(2:end)'*y0;
        if(length(B) > 1)
            y = y+B(2:end)'*x0;
        end
        
        x0(2:end) = x0(1:end-1);
        x0(1) = x(i);
        
        y0(2:end) = y0(1:end-1);
        y0(1) = y;
        
        res(i) = y;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%   RLS Idetification   %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inicjalizacja metody rls
function RLS = RLSInit(dB, dA, k, lambda)   
    RLS = struct('dA', dA, 'dB', dB, 'k', k, 'prevX', zeros(dB+k+1, 1), 'prevY', zeros(dA, 1), 'teta', zeros(dB + dA + 1, 1), 'lambda', lambda, 'P', eye(dA + dB + 1) * 100, 'iteration', 0);
end

% wykonanie kroku rls
function [res] = RLSStep(RLS, x, y)

% increment iteration nr
    RLS.iteration=RLS.iteration+1;
    
% update x vectors
    RLS.prevX(2:end) = RLS.prevX(1:end-1);
    RLS.prevX(1) = x;

% check if have enough samples
    vectX = RLS.prevX(RLS.k+1:end);

    if(RLS.iteration > max(RLS.dA, RLS.dB+RLS.k))
        fi = [ vectX' -RLS.prevY']';
        RLS.P = RLS.P - RLS.P*fi*(fi')*RLS.P/(RLS.lambda + (fi')*RLS.P*fi);
        
        wk = RLS.P*fi;
        ye = fi'*RLS.teta;
        e = y - ye;
        
% update estimates
        RLS.teta = RLS.teta + wk * e;
    end
    
% update y vectors
    RLS.prevY(2:end) = RLS.prevY(1:end-1);
    RLS.prevY(1) = y;
    
    res = RLS;
end

% wybranie z rls otrzymanych B i A
function [B A] = RLSData(rls)
    B = rls.teta(1:rls.dB+1);
    A = [1 rls.teta(rls.dB+2:end)']';
end