function lab()
    B = [0 0.2];
    A = [1 -0.8];
    x = rand(1000, 1);
    y = filter(B, A, x);
    
    RLMSInit(0, 1, 1, 1);
    
    for i =1:length(x)
        RLMSStep(x(i), y(i))
    end
end

function GPC(H, L, alfa, ro, w)
    one = zeros(L, 1);
    one(1) = 1;

    i = 0;
%     wyznaczenie odpowiedzi skokowej referencyjnego modelu
    step = ones(H, 1);
    modelB = [0.2];
    modelA = [1 -0.8];
    h = filter(B,A, step);
    
%     wyznaczenie wartosi Q
    Q = zeros(H, L);
    for i=1:H
        for j = 1:L
            index = i - j + 1;
            if(index < 1)
                index = 0;
            end
            
            Q(i, j) = h(index);
        end
    end
    
    w0 = w(i+1:i+H);
    
    q = one'*inv(Q'*Q + ro*eye(L))*(Q')
    du = q'*(w0 - y0);

end


function RLMSInit(dBa, dAa, ka, lambdaa)
    global dA;
    global dB;
    global k;
    global prevX;
    global prevY;
    global teta;
    global lambda;
    global P;
    global it;
    
% init globals
    dB = dBa;
    dA = dAa;
    k = ka;
    it = 0; %iteration number 
    lambda = lambdaa; %wsp. zapominania
    
% init vectors/matrixes
    prevX = zeros(dB+k+1, 1);
    prevY = zeros(dA, 1);
    teta = zeros(dB + dA + 1, 1);
    
    P = eye(dA + dB + 1) * 100;
    fi = zeros(dA + dB + 1, 1);
end

function [estimates] = RLMSStep(x, y)
    global dA;
    global dB;
    global k;
    global prevX;
    global prevY;
    global teta;
    global lambda;
    global P;
    global it;

% increment iteration nr
    it=it+1;
    
% update x vectors
    prevX(2:end) = prevX(1:end-1);
    prevX(1) = x;

% check if have enough samples
    vectX = prevX(k+1:end);

    if(it > max(dA, dB+k))
        fi = [ vectX' -prevY']';
        P = P - P*fi*(fi')*P/(lambda + (fi')*P*fi);
        
        wk = P*fi;
        ye = fi'*teta;
        e = y - ye;
        
% update estimates
        teta = teta + wk * e;
    end
    
% update y vectors
    prevY(2:end) = prevY(1:end-1);
    prevY(1) = y;
    
    estimates = teta;
  
end

% function [teta] = RLMSIdentification(dB, dA, k, x, y)
%     len = length(x);
%     start = max(dA, dB+k);
%     
%     teta = zeros(dB + dA + 1, len - start);
%     tetai = zeros(dB + dA + 1, 1);
%     prevY = zeros(dA);
%     prevX = zeros(dB + k);    
%     
%     
%     
%     P = eye(dA + dB + 1) * 100;
%     fi = zeros(dA + dB + 1, 1);
%     
%     for i=start+1:len
%         
%         prevY = fliplr(y(i - dA:i-1));
%         prevX = fliplr(x(i-dB-k:i-k));
%         
%         fi = [-prevY prevX]';
%         
%         P = P - P*fi*(fi')*P/(1 + (fi')*P*fi);
%         
%         
%         
%         wk = P*fi;
%  
%         ye = fi'*tetai;
%         e = lastY - ye;
%         
%         tetai = tetai + wk * e;
%         
%         teta(1:length(tetai), i-start) = tetai(1:end);
%     end
% 
% end